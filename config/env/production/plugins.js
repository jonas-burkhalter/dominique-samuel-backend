console.info(
  "connect space (production)",
  `${process.env.DO_SPACE_ENDPOINT} ${process.env.DO_SPACE_BUCKET} with access-key: ${process.env.DO_SPACE_ACCESS_KEY}`
);

console.info(
  "email (production)",
  `
    ${process.env.DO_SPACE_SMTP_HOST} 
    ${process.env.DO_SPACE_SMTP_PORT} 
    ${process.env.DO_SPACE_SMTP_USERNAME} 
    ${process.env.DO_SPACE_SMTP_PASSWORD}
  `
);

module.exports = {
  email: {
    config: {
      provider: 'nodemailer',
      providerOptions: {
        host: process.env.DO_SPACE_SMTP_HOST,
        port: process.env.DO_SPACE_SMTP_PORT,
        auth: {
          user: process.env.DO_SPACE_SMTP_USERNAME,
          pass: process.env.DO_SPACE_SMTP_PASSWORD,
        },
      },
      settings: {
        defaultFrom: 'trauzeugen.dominique.samuel@gmail.com',
        defaultReplyTo: 'trauzeugen.dominique.samuel@gmail.com',
      },
    },
  },

  upload: {
    config: {
      provider: "strapi-provider-upload-do",
      providerOptions: {
        key: process.env.DO_SPACE_ACCESS_KEY,
        secret: process.env.DO_SPACE_SECRET_KEY,
        endpoint: process.env.DO_SPACE_ENDPOINT,
        space: process.env.DO_SPACE_BUCKET,
        directory: process.env.DO_SPACE_DIRECTORY,
        cdn: process.env.DO_SPACE_CDN,
      },
    },
  },
};
