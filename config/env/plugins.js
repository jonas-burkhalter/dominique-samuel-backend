// console.info(
//     "connect space (production)",
//     `${process.env.DO_SPACE_ENDPOINT} ${process.env.DO_SPACE_BUCKET} with access-key: ${process.env.DO_SPACE_ACCESS_KEY}`
// );

// console.info(
//   "email",
//   `
//     ${process.env.DO_SPACE_ACCESS_KEY_SMTP_HOST} 
//     ${process.env.DO_SPACE_ACCESS_KEY_SMTP_PORT} 
//     ${DO_SPACE_ACCESS_KEY_SMTP_USERNAME} 
//     ${process.env.DO_SPACE_ACCESS_KEY_SMTP_PASSWORD}
//   `
// );

module.exports = ({ env }) => ({
  email: {
    config: {
      provider: 'nodemailer',
      providerOptions: {
        host: env('SMTP_HOST', 'smtpauths.bluewin.ch'),
        port: env('SMTP_PORT', 465),
        auth: {
          user: env('SMTP_USERNAME', 'jonas.burkhalter@bluewin.ch'),
          pass: env('SMTP_PASSWORD', '********'),
        },
      },
      settings: {
        defaultFrom: 'jonas.burkhalter@bluewin.ch',
        defaultReplyTo: 'jonas.burkhalter@bluewin.ch',
      },
    },
  },
});