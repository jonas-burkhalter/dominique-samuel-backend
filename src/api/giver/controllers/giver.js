'use strict';

/**
 * giver controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::giver.giver');
