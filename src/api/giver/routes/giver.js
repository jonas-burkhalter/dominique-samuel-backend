'use strict';

/**
 * giver router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::giver.giver');
