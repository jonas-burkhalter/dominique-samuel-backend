'use strict';

/**
 * giver service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::giver.giver', ({ strapi }) => ({
    async create(params) {
        const result = await super.create(params);

        const giver = await strapi.entityService.findOne('api::giver.giver', result.id, {
            populate: { article: true },
        });

        await sendToGiver(strapi, giver);

        return result;
    }
}))

async function sendToGiver(strapi, giver) {
    await strapi.plugins['email'].services.email.send({
        to: giver.mail,
        bcc: getBcc(giver),
        from: 'trauzeugen.dominique.samuel@gmail.com',
        replyTo: 'trauzeugen.dominique.samuel@gmail.com',
        subject: 'Hochzeit',
        text: getText(giver),
    });
}

function getBcc(giver) {
    if (giver.article?.category === 'beitrag') {
        return 'trauzeugen.dominique.samuel@gmail.com';
    } else {
        return 'sam.hadorn@gmail.com';
    }
}

function getText(giver) {
    switch (giver.article?.category) {
        case 'anteil':
            return partText(giver);
        case 'beitrag':
            return beitragText(giver);
        default:
            return bringText(giver);
    }
}

function beitragText(giver) {
    return `
    Hallo ${giver.name}
    <br>
    <br>Besten Dank für den Beitrag. Damit Dominique und Samuel nicht alle Beiträge kennen und es eine Überraschung wird, läuft der Kontakt für den Beitrag über uns.
    <br>Bitte sendet uns, einen ungefähren Ablauf inkl. Zeitplanung. So können wir euren Beitrag am besten einplanen. Falls konkrete Wünsche dazu gehören, wie Tageszeit, benötigte Infrastruktur und so weiter, schreibt alles in das Mail.
    <br>
    <br>Wir und das Brautpaar freuen uns bereits.
    <br>Grüsse Markus und Michael`;
}

function bringText(giver) {
    return `
        Hallo ${giver.name}
        <br>
        <br>Herzlichen Dank für dein Geschenk ${giver.article?.title} aus unserer Wunschliste!
        <br>
        <br>Es freut uns, dass du dich für ein Geschenk zum Mitbringen entschieden hast. Bitte bringe dieses an unsere Hochzeit mit und lege es dort an den dafür vorgesehenen Platz (z. B. Geschenketisch, -auto, ..) hin.
        <br>Der angegebene Preis entspricht ungefähr dem Betrag, den wir bei einer kurzen Recherche zum Produkt gefunden haben und soll als Richtwert dienen.
        <br>
        <br>Danke viel Mals, liebe Grüsse und bis bald.
        <br>Dominique und Samuel`;
}


function partText(giver) {
    return `
        Hallo ${giver.name}
        <br>
        <br>Herzlichen Dank für deine CHF ${giver.part} für das Geschenk ${giver.article?.title} aus unserer Wunschliste!
        <br>
        <br>Bitte bringe den gewählten Betrag in einem Couvert mit an die Hochzeit oder überweise den Betrag mit dem Betreff "Hochzeit, ${giver.article?.title}" auf folgendes Konto:
        <br>
        <br>UBS Switzerland AG
        <br>Samuel Hadorn
        <br>CH22 0026 2262 4583 7140 M
        <br>
        <br>Danke viel Mals, liebe Grüsse und bis bald.
        <br>Dominique und Samuel`;
}

